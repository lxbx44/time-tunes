// Hours
let hUp: HTMLButtonElement | null = document.querySelector('#hours-up');
let hDown: HTMLButtonElement | null = document.querySelector('#hours-down');
export let hTxt: HTMLInputElement | null = document.querySelector('#hours-input');

hUp?.addEventListener('click', () => {
    let val: number | undefined = hTxt?.valueAsNumber;
    if (val === undefined) {
        val = 1;
    } else {
        val += 1;
    }
    if (hTxt) {
        hTxt.value = val.toString();
    }
});

hDown?.addEventListener('click', () => {
    let val: number | undefined = hTxt?.valueAsNumber;
    if (val === undefined) {
        val = 0;
    } else if (val === 0) {
        val = 0;
    } else {
        val -= 1;
    }
    if (hTxt) {
        hTxt.value = val.toString();
    }
});

// Minutes
let mUp: HTMLButtonElement | null = document.querySelector('#minutes-up');
let mDown: HTMLButtonElement | null = document.querySelector('#minutes-down');
export let mTxt: HTMLInputElement | null = document.querySelector('#minutes-input');

mUp?.addEventListener('click', () => {
    let val: number | undefined = mTxt?.valueAsNumber;
    if (val === undefined) {
        val = 1;
    } else if (val === 59) {
        let new_h_value: number | undefined = hTxt?.valueAsNumber;
        if (new_h_value === undefined) {
            new_h_value = 0;
        }
        new_h_value += 1;
        val = 0;
        if (hTxt) {
            hTxt.value = new_h_value.toString();
        }
    } else {
        val += 1;
    }
    if (mTxt) {
        mTxt.value = val.toString();
    }
});

mDown?.addEventListener('click', () => {
    let val: number | undefined = mTxt?.valueAsNumber;
    if (val === undefined) {
        val = 0;
    } else if (val === 0) {
        val = 0;
    } else {
        val -= 1;
    }
    if (mTxt) {
        mTxt.value = val.toString();
    }
});

// Seconds
let sUp: HTMLButtonElement | null = document.querySelector('#seconds-up');
let sDown: HTMLButtonElement | null = document.querySelector('#seconds-down');
export let sTxt: HTMLInputElement | null = document.querySelector('#seconds-input');

// Does this have to be nested like this??
sUp?.addEventListener('click', () => {
    let val: number | undefined = sTxt?.valueAsNumber;
    let new_m_value: number | undefined = mTxt?.valueAsNumber;
    
    if (val === undefined) {
        val = 1;
    } else if (val === 59) {
        if (new_m_value === undefined) {
            new_m_value = 1;
        } else if (new_m_value === 59) {
            let new_h_value: number | undefined = hTxt?.valueAsNumber;
            if (new_h_value === undefined) {
                new_h_value = 0;
            }
            new_h_value += 1;
            new_m_value = 0;
            if (hTxt) {
                hTxt.value = new_h_value.toString();
            }
            val = 0;
        } else {
            new_m_value += 1;
            val = 0;
        }

        if (mTxt) {
            mTxt.value = new_m_value.toString();
        }
        val = 0;
    } else {
        val += 1;
    }
    if (sTxt) {
        sTxt.value = val.toString();
    }
});

sDown?.addEventListener('click', () => {
    let val: number | undefined = sTxt?.valueAsNumber;
    if (val === undefined) {
        val = 0;
    } else if (val === 0) {
        val = 0;
    } else {
        val -= 1;
    }

    if (sTxt) {
        sTxt.value = val.toString();
    }
});

function preventNonNumericInput(e: Event) {
    let inputElement = e.target as HTMLInputElement;

    try {
        inputElement.valueAsNumber;
    } catch {
        inputElement.value = '0';
    }
}

function preventOverflow(event: Event) {
    let inputElement = event.target as HTMLInputElement;
    if (inputElement.valueAsNumber > 59) {
        inputElement.value = "59";
    }
}

hTxt?.addEventListener('input', preventNonNumericInput);
mTxt?.addEventListener('input', preventNonNumericInput);
sTxt?.addEventListener('input', preventNonNumericInput);

mTxt?.addEventListener('input', preventOverflow);
sTxt?.addEventListener('input', preventOverflow);
