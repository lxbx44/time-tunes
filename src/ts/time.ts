
export interface Time {
    hours: number,
    minutes: number,
    seconds: number,
    toSeconds(): number,
}

export class Time implements Time {
    constructor(public hours: number, public minutes: number, public seconds: number) {}

    toSeconds(): number {
        return this.hours * 60 * 60 + this.minutes * 60 + this.seconds;
    }

    static fromSeconds(seconds: number): Time {
        const hours = Math.floor(seconds / 3600);
        const minutes = Math.floor((seconds % 3600) / 60);
        const remainingSeconds = seconds % 60;

        return new Time(hours, minutes, remainingSeconds);
    } 

    humanReadable(): string {
        const formattedMinutes = this.padZero(this.minutes);
        const formattedSeconds = this.padZero(this.seconds);

        return `${this.hours}:${formattedMinutes}:${formattedSeconds}`;
    }

    private padZero(value: number): string {
        return value < 10 ? `0${value}` : `${value}`;
    }
}
