import { invoke } from "@tauri-apps/api/tauri";
import { open } from "@tauri-apps/api/dialog";

import { hTxt, mTxt, sTxt } from "./duration_select.ts";
import { Time } from "./time.ts"
import { maincontent, stepsFactorVal, depthFactorVal, loopsVal } from "./settings.ts"
import { get_metadata } from "./metadata.ts";


let dirButton: HTMLButtonElement | null = document.querySelector('#choose-dir');

let dirButtonText: string = "Choose directory";
let musicDir: string;
let musicPath: string | string[] | null;

dirButton?.addEventListener('click', async () => {
    musicPath = await open({
        multiple: false,
        directory: true,
        filters: [{
            name: 'Music', 
            extensions: []
        }]
    });

    if (musicPath === null) {
        console.error('Error: no directory was selected');
    } else {
        if (dirButton) {
            dirButton.textContent = musicPath.toString();
            musicDir = musicPath.toString();
        }
    }
});

dirButton?.addEventListener('mouseenter', () => {
    if (dirButton) {
        dirButton.textContent = dirButtonText;
    }
});

dirButton?.addEventListener('mouseleave', () => {
    if (dirButton) {
        if (musicDir) {
            dirButton.textContent = musicDir;
        } else {
            dirButton.textContent = dirButtonText;
        }
    }
});

let form: HTMLFormElement | null = document.querySelector('#main-form');

form?.addEventListener('submit', (event: Event) => {
    event.preventDefault();

    if (
        hTxt?.valueAsNumber === undefined 
            || mTxt?.valueAsNumber === undefined 
                || sTxt?.valueAsNumber === undefined
    ) {
        return;
    }

    let duration: Time = new Time(
        hTxt.valueAsNumber,
        mTxt.valueAsNumber,
        sTxt.valueAsNumber
    );

    let dirPath: string | string[] | null = musicPath;

    if (dirPath === null || dirPath === undefined) {
        return;
    }

    let loader: HTMLElement | null = document.querySelector('#loader');

    if (maincontent) {
        maincontent.style.display = 'none';
    }
    if (loader) {
        loader.style.display = 'grid';
    }


    invoke('get_playlist', {
        time: duration.toSeconds(),
        path: dirPath,
        depthVal: depthFactorVal,
        stepsVal: stepsFactorVal,
        loopsVal: loopsVal,
    }).then((s: [Array<[string, string]>, number] | unknown) => {
        let [songs_path, total_time] = s as [string[], number];

        if (loader) {
            loader.style.display = 'none';
        }

        let songsDiv: HTMLDivElement | null = document.querySelector('#putsongshere');
        let songsDivParent: HTMLDivElement | null = document.querySelector('.confirm');
        let timespan: HTMLSpanElement | null = document.querySelector('#changetotaltime');
        let timespanreal: HTMLSpanElement | null = document.querySelector('#changerealtime');

        if (timespan && timespanreal) {
            timespan.textContent = Time.fromSeconds(total_time).humanReadable();
            timespanreal.textContent = duration.humanReadable();
        }
 
        let n: number = 1;
        songs_path.forEach((song: string) => {
            let p = document.createElement('p');
            p.textContent = `${n}. ${song.split('/').slice(-1)[0]}`;
            songsDiv?.appendChild(p);
            n++;
        });

        if (songsDivParent) {
            songsDivParent.style.display = 'grid';
        }

        let cancelBtn: HTMLButtonElement | null = document.querySelector('#confirm-stop');
        let continueBtn: HTMLButtonElement | null = document.querySelector('#confirm-ok');

        cancelBtn?.addEventListener('click', () => {
            window.location.reload();
        });

        continueBtn?.addEventListener('click', async () => {
            if (songsDivParent) {
                songsDivParent.style.display = 'none';
            }

            //invoke('play', { playlist: songs_path });

            for (let song of songs_path) {
                let t: number = 0;
                t = await get_metadata(song);
                console.log(t)
                //await wait(1);
            }
        });
    })
    .catch((e) => {
        //window.location.reload();
        console.error('error\n', e);
    });
});

/**
 * @param {number}  t - Time to wait in seconds.
 * @returns {Promise<void>} Just waits
 */
async function wait(t: number): Promise<void> {
    let ms = t * 1000;
    return new Promise( resolve => setTimeout(resolve, ms) );
}
