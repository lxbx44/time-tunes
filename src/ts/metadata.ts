
import { invoke } from "@tauri-apps/api/tauri";

import { Time } from './time';

export async function get_metadata(song: string): Promise<number> {
    let totalTime: number = -1;
    invoke('get_metadata', { path: song }).then((metadata) => {
        let [title, artist, album, picture, mimetype, total_time] = metadata as [string, string, string, Uint8Array | null, string, number];

        totalTime = total_time;

        let img: HTMLImageElement | null = document.querySelector('#d-album');

        if (img) {
            if (picture === null) {
                img.src = 'src/assets/images/default_album_cover.svg';
            } else {
                img.onerror = () => {
                    console.error('Error loading image:', img?.src);
                };

                img.src = URL.createObjectURL(
                    new Blob([picture], { type: mimetype })
                );
            }
        }

        let hTitle: HTMLElement | null = document.querySelector('#d-title');
        if (hTitle) {
            hTitle.textContent = title;
        }

        let hArtist: HTMLElement | null = document.querySelector('#d-artist');
        if (hArtist) {
            hArtist.textContent = artist;
        }

        let hAlbum: HTMLElement | null = document.querySelector('#d-album-name');
        if (hAlbum) {
            hAlbum.textContent = album;
        }

        let dTotal_time: HTMLElement | null = document.querySelector('#d-total-time');
        if (dTotal_time) {
            dTotal_time.textContent = Time.fromSeconds(total_time).humanReadable();
        }

        let display: HTMLDivElement | null = document.querySelector('.display');
        if (display) {
            display.style.display = 'grid';
        }
    })
    return totalTime;
}
