export let maincontent: HTMLElement | null = document.querySelector('.container');

let settingsContent: HTMLElement | null = document.querySelector('.settings');
let settingsBtn: HTMLElement | null = document.querySelector('#open-settings');

export let stepsFactorVal: number = 20;
export let depthFactorVal: number = 20;
export let loopsVal: number = 1;


settingsBtn?.addEventListener('click', () => {
    if (maincontent) {
        maincontent.style.display = 'none';
    }
    if (settingsContent) {
        settingsContent.style.display = 'flex'; 
    }

    // DEPTH FACTOR

    let depthFactor: HTMLElement | null = document.querySelector('#depth-factor-num');
    let depthFactorSlider: HTMLInputElement | null = document.querySelector('#depth-factor');

    if (depthFactor && depthFactorSlider) {
        depthFactor.textContent = depthFactorSlider.value + '%';
        depthFactorVal = depthFactorSlider.valueAsNumber;
    };
    
    depthFactorSlider?.addEventListener('input', () => {
        if (depthFactor && depthFactorSlider) {
            depthFactor.textContent = depthFactorSlider.value + '%';
            depthFactorVal = depthFactorSlider.valueAsNumber;
        };
    });

    // STEPS FACTOR

    let stepsFactor: HTMLElement | null = document.querySelector('#steps-factor-num');
    let stepsFactorSlider: HTMLInputElement | null = document.querySelector('#steps-factor');

    if (stepsFactor && stepsFactorSlider) {
        stepsFactor.textContent = stepsFactorSlider.value + '%';
        stepsFactorVal = stepsFactorSlider.valueAsNumber;
    };
    
    stepsFactorSlider?.addEventListener('input', () => {
        if (stepsFactor && stepsFactorSlider) {
            stepsFactor.textContent = stepsFactorSlider.value + '%';
            stepsFactorVal = stepsFactorSlider.valueAsNumber;
        };
    });

    // STEPS FACTOR

    let loops: HTMLInputElement | null = document.querySelector('#loops');

    loops?.addEventListener('input', () => {
        if (loops) {
            loopsVal = loops.valueAsNumber;
        };
    });


    let closeSettings: HTMLElement | null = document.querySelector('#close-settings');

    closeSettings?.addEventListener('click', () => {
        if (maincontent) {
            maincontent.style.display = 'grid';
        }
        if (settingsContent) {
            settingsContent.style.display = 'none'; 
        }
    });
});
